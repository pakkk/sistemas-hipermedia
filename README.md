# Sistemas Hipermedia - Trabajo Final

- Docker
- WordPress
- MySql
- PhpMyAdmin
- WooCommerce

## Setup
Arrancar este comando con root
```bash
docker-compose -f docker-compose.yml up -d 
```

*WordPress* estará disponible en [http://localhost:8020](http://localhost:8020)

*[phpMyAdmin](https://github.com/phpmyadmin/phpmyadmin)*: Puedes acceder a PhpMyAdmin en [http://localhost:8183](http://localhost:8183)
```shell script
port: mysql:3306
username: root
password: root
``` 

La imagen docker de PhpMyAdmin docker está configurada con el usuario `root`. Por otro lado, debes asignar una contraseña a la base de datos MySQL a través del DockerFile